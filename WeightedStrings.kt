/**
 * Created by fadhifatah on 2023/09/18
 */

fun main(args: Array<String>) {
    // Run main()

    // Input:
    // abbcccd
    // 1 3 9 8

    // Explanation:
    // First input is a string, for example: "abbcccd"
    // Second input is a array of queries that separated with space, for example: 1 3 9 8

    // Output:
    // [Yes, Yes, Yes, No]

    val readString = readln().trim()
    val readQueries = readln().trim().split(" ").map { it.toInt() }.toTypedArray()

    println(findStatusOfQueries(readString, readQueries))
}

fun findStatusOfQueries(input: String, queries: Array<Int>): List<String> {
    val const = 96 // To find char value a = (97 - 96) = 1, b = (98 - 96) = 2, etc.

    var tempChar: Char? = null // Store current char from input for validation
    var tempCount = 0 // Store current count of tempChar

    val weightedStrings = hashSetOf<Int>() // All of weighted strings stored here. Using HashSet to improve performance

    // Loops to fill the weighted strings
    for (char in input) {
        val charValue = char.code - const

        if (char == tempChar) { // If previous char is the same as right now, combine and calculate its weighted strings
            tempCount+=1
            weightedStrings.add(tempCount * (charValue))
        } else { // New char, new count, new weighted strings item
            tempChar = char
            tempCount = 1
            weightedStrings.add(charValue)
        }
    }

    return queries.map { if (it in weightedStrings) "Yes" else "No" }
}
