/**
 * Created by fadhifatah on 2023/09/18
 *
 * input: String (for example: "3943". Consider it may receive
 * single number "3", empty string ""
 *
 * k: Int (for example: 1. It indicates chance of "replacement"
 * of the character number in the input occurs. Consider it may
 * receive any number and also it does not need to reach 0 when
 * used. So, the maximum usage should be input.size / 2)
 *
 * Consider that the "replacement" is a swap feature of its
 * palindrome position. For example: In "1212", index position
 * of 0 and 3 will be replacing each other in order to find the
 * max value. Also the same as index of 1 and 2, so that the
 * result will be "2222".
 *
 */
fun main(args: Array<String>) {
    val input = readln().trim().map { it.digitToInt() }.toTypedArray()
    val k = readln().trim().toInt()

    if (findHighestPalindrome(input, 0, input.size - 1, k)) {
        println(input.joinToString(""))
    } else {
        println(-1)
    }
}

fun findHighestPalindrome(arr: Array<Int>, start: Int, end: Int, chance: Int): Boolean {
    var tempChance = chance

    if (start > end) return false // Indicates empty String or invalid input

    if (start == end) return true // Check if it reaches the center

    if (arr[start] != arr[end]) {
        if (chance <= 0) return false // There is no chance to fix into the palindrome

        if (arr[start] > arr[end]) {
            arr[end] = arr[start]
        } else {
            arr[start] = arr[end]
        }

        tempChance-- // Chance is used, reduce by 1
    }

    if (end - start > 1) return findHighestPalindrome(arr, start + 1, end - 1, tempChance) // Continue

    return true
}