# HITOPIA - Problem Solving Test

## Submission Explanation

### Weighted Strings

```
Input:
abbcccd
1 3 9 8

Output:
[Yes, Yes, Yes, No]

===
"abbcccd" has a weighted strings of [1, 2, 4, 3, 6, 9, 4] can be optimized using Set to reduce repetitive item

Queries of [1, 3, 9, 8] that match with the weighted strings is [1, 3, 9] 

Thus, the result should be [Yes, Yes, Yes, No] respectively
```

### Highest Palindrome

```
Input:
3943 // input
1    // chance

Output:
3993

===
input: String (for example: "3943". Consider it may receive single number "3", empty string ""

k: Int (for example: 1. It indicates chance of "replacement" of the character number in the input occurs. Consider it may receive any number and also it does not need to reach 0 when used. So, the maximum usage should be input.size / 2)

Consider that the "replacement" is a swap feature of its palindrome position. For example: In "1212", index position of 0 and 3 will be replacing each other in order to find the max value. Also the same as index of 1 and 2, so that the result will be "2222".

In input = "3943" and k = 1. Index 0 and 3 is already palindrome so it may be skipped. Index 1 and 2 is not a palindrome, so it need to be fixed by finding the max value which is 9 (9 > 4). Thus, the result is "3993"

```

### Balanced Bracket

```
Input:
{ [ ( ) ] }

Output:
YES

===
"{ [ ( ) ] }" passed as trimmed String into isItBalanced() function. The input parameter in isItBalanced() should be "{[()]}". Every char in the input is looped to be validated whether it is balanced or not. Return "YES" when it does, "NO" when it doesn't.

Before that, reserved a single ArrayList, to store the bracket state, as a Stack representative. This problem is a Stack simulation to get the list filled and then emptied at the end.

Every bracket opening, the stack will be pushed with its char of corresponding bracket. Thus, every bracket closing, the stack will be poped based on corresponding bracket.

In isItBalanced() function, every action of add() (push), removeLast() (pop), isEmpty(), last() (peek) is an O(1) time (https://www.baeldung.com/java-collections-complexity#arraylist). To validate whether the sequence of brackets is balanced or not, it runs through a loop which is an O(n) time. Combine all of that, 1 each action for n items, we should get O(n) respectfully.

Each operation such as add(), when handling the opening bracket, needs 1 complexity. In the worst scenario, when handling the closing bracket, it requires isEmpty(), last() and removeLast() which needs 3 complexity. Then, one operation is equals to

T(n) = 1 + 3 = O(3)

Next, because it runs in loops until n brackets, it needs n complexity. Then, the whole operations of isItBalanced() is equals to

T(n) = 3n = O(n)
```


