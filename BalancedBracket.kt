/**
 * Created by fadhifatah on 2023/09/18
 */

fun main(args: Array<String>) {
    // read input, so it may typed with/without white space
    val input = readln().trim().replace(Regex("\\s"), "")

    println(isItBalanced(input))
}

fun isItBalanced(input: String): String {
    // bracket stacks representative
    val bracketsValidator = arrayListOf<Char>()

    for (bracket in input) {
        // the opening
        if (bracket == '{' || bracket == '[' || bracket == '(') bracketsValidator.add(bracket) // push current bracket

        // the closing
        if (bracket == '}') {
            // pop curly whether it is valid or not: valid when previously is the opening curly only
            if (bracketsValidator.isEmpty() || bracketsValidator.last() != '{') {
                return "NO"
            }

            bracketsValidator.removeLast() // equals to pop
        }
        // pop square whether it is valid or not: valid when previously is the opening square only
        if (bracket == ']') {
            if (bracketsValidator.isEmpty() || bracketsValidator.last() != '[') {
                return "NO"
            }

            bracketsValidator.removeLast() // equals to pop
        }
        // pop round whether it is valid or not: valid when previously is the opening round only
        if (bracket == ')') {
            if (bracketsValidator.isEmpty() || bracketsValidator.last() != '(') {
                return "NO"
            }

            bracketsValidator.removeLast() // equals to pop
        }
    }

    // if it is empty which means there is no brackets left behind, then the brackets is respectfully balanced
    return if (bracketsValidator.isEmpty()) "YES" else "NO"
}